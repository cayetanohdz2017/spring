<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Getion Equipos Futbol</h1>

<br>

<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->


<div class="span3">
    <form action="guardar" method="post">
    <label>Id</label>
    <input type="text" name="id" class="span3">
    <label>Nombre</label>
    <input type="text" name="nombre" class="span3">
    <label>Pais</label>
    <input type="text" name="pais" class="span3">
    <label>Liga</label>
    <input type="text" name="liga" class="span3">
    <input type="submit" name="btnGuardar" value="guardar" class="btn btn-success pull-right">
    <input type="submit" name="btnGuardar" value="eliminar" class="btn btn-danger pull-right">
    <input type="submit" name="btnGuardar" value="editar" class="btn btn-warning pull-right">
    <div class="clearfix"></div>
    </form>
</div>

<div class="container">
	<div class="row">
		<div class="span5">
            <table class="table table-striped table-condensed">
                  <thead>
                  
                  <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                      <th>Pais</th>
                      <th>Liga</th>                                          
                  </tr>
              </thead>   
              <tbody>
              <c:forEach items="${lista}" var ="t">
	                <tr>
	                	<td>${t.getId()}</td>
	                    <td>${t.getNombre()}</td>
	                    <td>${t.getPais()}</td>
	                    <td>${t.getLiga()}</td>
	                </tr>
               </c:forEach>                                    
              </tbody>
            </table>
            </div>
	</div>
</div>


</body>
</html>