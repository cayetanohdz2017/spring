package com.mx.general;

import java.util.List;

public interface Metodos {
	public void guardar(Object obj);
	public void eliminar(Object obj);
	public void editar(Object obj);
	public Object buscar(Object obj);
	public List mostrar();
	//public void buscarN(Object obj);
	//public void validarD(Object obj);

}
