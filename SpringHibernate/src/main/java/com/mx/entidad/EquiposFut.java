package com.mx.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EQUIPOS_FUT")
public class EquiposFut {
	
	@Id
	@Column(name = "ID" , columnDefinition = "NUMBER")
	int id;
	
	@Column(name = "NOMBRE" , columnDefinition = "NVARCHAR(100)")
	String nombre;
	
	@Column(name = "PAIS" , columnDefinition = "NVARCHAR(100)")
	String pais;
		
	@Column(name = "LIGA" , columnDefinition = "NVARCHAR(100)")
	String liga;
		
	public EquiposFut() {}
	
	public EquiposFut(int id) {
		this.id = id;
	}

	public EquiposFut(int id, String nombre, String pais, String liga) {
		this.id = id;
		this.nombre = nombre;
		this.pais = pais;
		this.liga = liga;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getLiga() {
		return liga;
	}

	public void setLiga(String liga) {
		this.liga = liga;
	}

	@Override
	public String toString() {
		return "EquiposFut [id=" + id + ", nombre=" + nombre + ", pais=" + pais + ", liga=" + liga + "]\n";
	}


}
