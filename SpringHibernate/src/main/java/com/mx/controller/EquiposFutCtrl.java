package com.mx.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.mx.dao.DaoEquipoFut;
import com.mx.entidad.EquiposFut;

@Controller
@RequestMapping(value="Equipos")
public class EquiposFutCtrl {
	
	DaoEquipoFut dao=null;
	EquiposFut fut= null;
	@RequestMapping(value="/inicio", method = RequestMethod.GET)
	public ModelAndView inicio() {
		//ModelAndView view = new ModelAndView("equipoVista");
		//return view;
		dao = new DaoEquipoFut();
		List<EquiposFut> lista = dao.mostrar();
		
		return new ModelAndView("equipoVista", "lista", lista);
	}
	
	@RequestMapping(value="/guardar", method = RequestMethod.POST)
	public ModelAndView guardar(@RequestParam("id") String id, @RequestParam("nombre") String nombre,
								@RequestParam("pais") String pais, @RequestParam("liga") String liga,
								@RequestParam String btnGuardar) {
		dao = new DaoEquipoFut();
		fut = new EquiposFut();
		fut.setId(Integer.parseInt(id));
		fut.setNombre(nombre);
		fut.setPais(pais);
		fut.setLiga(liga);
		
		if(btnGuardar.equals("guardar")) {
			dao.guardar(fut);
			System.out.println("guardar");
		}else if(btnGuardar.equals("eliminar")) {
			dao.eliminar(fut);
			System.out.println("eliminar");
		}else if(btnGuardar.equals("editar")) {
			dao.editar(fut);
			System.out.println("editar");
		}
		
		List<EquiposFut> lista = dao.mostrar();
		
		return new ModelAndView("equipoVista", "lista", lista);
		
		
	}

}
