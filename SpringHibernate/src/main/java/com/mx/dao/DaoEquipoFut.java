package com.mx.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mx.entidad.EquiposFut;
import com.mx.general.Metodos;

public class DaoEquipoFut implements Metodos{
	EntityManagerFactory emf=Persistence.createEntityManagerFactory("Equipos");
	EntityManager em= emf.createEntityManager();
	EquiposFut fut = null;
	
	@Override
	public void guardar(Object obj) {
		// TODO Auto-generated method stub
		fut = (EquiposFut) obj;
		
		em.getTransaction().begin();
		
		try {
			em.persist(fut);
			em.getTransaction().commit();
			System.out.println("Se agrego el equipo!!!");
		}catch(Exception e) {
			em.getTransaction().rollback();
			System.out.println("Error guardar->"+e.getMessage());
		}
		//em.close();
		
	}

	@Override
	public void eliminar(Object obj) {
		// TODO Auto-generated method stub
		fut = (EquiposFut) obj;
		fut= em.find(EquiposFut.class, fut.getId());
		em.getTransaction().begin();
		try {
			em.remove(fut);
			em.getTransaction().commit();
			System.out.println("Se elimino el equipo!!!");
		}catch(Exception e) {
			em.getTransaction().rollback();
			System.out.println("Error eliminar->"+e.getMessage());
		}
		//em.close();
		
		
	}

	@Override
	public void editar(Object obj) {
		// TODO Auto-generated method stub
		fut = (EquiposFut) obj;
		
		EquiposFut fute= em.find(EquiposFut.class, fut.getId());
		em.getTransaction().begin();
		try {
			fute.setNombre(fut.getNombre());
			fute.setPais(fut.getPais());
			fute.setLiga(fut.getLiga());
			em.getTransaction().commit();
			System.out.println("Se edito el equipo!!!");
		}catch(Exception e) {
			em.getTransaction().rollback();
			System.out.println("Error editar->"+e.getMessage());
		}
		//em.close();
	}

	@Override
	public Object buscar(Object obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List mostrar() {
		// TODO Auto-generated method stub
		
		return em.createQuery("from EquiposFut").getResultList();
	}

}
